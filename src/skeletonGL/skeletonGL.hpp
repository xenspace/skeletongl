//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

/**
 * @file    skeletonGL.hpp
 * @author  AlexHG
 * @date    9/4/2018
 * @version 1.0
 *
 * @brief Monolithic include file for lazy people
 * @section DESCRIPTION
 *
 * Includes all the SkeletonGL files in a single header file, the library is pretty small
 * so you might as well.
 */


#ifndef SRC_SKELETONGL_SKELETONGL_H
#define SRC_SKELETONGL_SKELETONGL_H


// General utility
#include "utility/SGL_AssetManager.hpp"
#include "utility/SGL_DataStructures.hpp"
#include "utility/SGL_Utility.hpp"
// Rendering
#include "renderer/SGL_OpenGLManager.hpp"
#include "renderer/SGL_Camera.hpp"
#include "renderer/SGL_PostProcessor.hpp"
#include "renderer/SGL_Shader.hpp"
#include "renderer/SGL_Renderer.hpp"
#include "renderer/SGL_Texture.hpp"
// Window management
#include "window/SGL_Window.hpp"

#endif // SRC_SKELETONGL_SKELETONGL_H
