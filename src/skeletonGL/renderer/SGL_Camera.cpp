//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

/**
 * @file    src/skeletonGL/utility/SGL_PostProcessor.hpp
 * @author  AlexHG
 * @date    9/4/2018
 * @version 1.0
 *
 * @brief Processes the frame's final render
 *
 * @section DESCRIPTION
 *
 * See the post processor shader for more filter and effects examples
 */

#include "SGL_Camera.hpp"

/**
 * @brief Main and only constructor
 *
 * @param oglm Main window SGL_OpenGLManager
 * @return nothing
 */
SGL_Camera::SGL_Camera(std::shared_ptr<SGL_OpenGLManager> oglm) : pScale(1.0f), pCameraMatrix(1.0f), pUpdateMatrix(true),
                       pOrthographicProjection(1.0f), pScreenW(0), pScreenH(0),
                       pCameraPos(0.0f), pDeltaCameraPos(0.0f), pDeltaScale(0.0f),
                       pOverlayMatrix(0.0f)
{
    // Share the main OGLM instance
    WMOGLM = oglm;
}

/**
 * @brief Destructor
 *
 * @return nothing
 */
SGL_Camera::~SGL_Camera()
{

}


/**
 * @brief Starts the camera
 *
 * @param windoW Camera width
 * @param windoH Camera height
 * @return nothing
 */
void SGL_Camera::initialize(int windowW, int windowH)
{
    this->pScreenW = windowW;
    this->pScreenH = windowH;
    this->pOrthographicProjection = glm::ortho(0.0f, static_cast<float>(pScreenW), static_cast<float>(pScreenH), 0.0f, -1.0f, 1.0f);
    this->pOverlayMatrix = pOrthographicProjection;

    this->pCameraPos = glm::vec2(pScreenW / 2, pScreenH / 2);
    this->pDeltaCameraPos = pCameraPos;
}


/**
 * @brief Scales the camera's 'Z' value
 *
 * @param newScale Float with the new value (-1.0 <-> 1.0)
 * @return nothing
 */
void SGL_Camera::setScale(float newScale)
{
    this->pScale = newScale;
}


/**
 * @brief Updates the camera's matrices (if needed or forced)
 *
 * @param forceUpdate Bool to force the new values on the matrices
 * @return nothing
 */
void SGL_Camera::updateCamera(bool forceUpdate)
{
    if (pDeltaCameraPos != pCameraPos || pDeltaScale != pScale)
        pUpdateMatrix = true;
    if (pUpdateMatrix || forceUpdate)
    {
        // Update the camera
        // Move the camera
        glm::vec3 translate(-pCameraPos.x + (pScreenW / 2), -pCameraPos.y + (pScreenH / 2), 0.0f);
        pCameraMatrix = glm::translate(pOrthographicProjection, translate);
        // Scale the camera
        glm::vec3 scale(pScale, pScale, 0.0f);
        pCameraMatrix = glm::scale(glm::mat4(1.0f), scale) * pCameraMatrix;
        // Note that the pActiveCamera must be set to true and cameraMode must be called before every render for the update to have effect
        pDeltaCameraPos = pCameraPos;
        pDeltaScale = pScale;
        pUpdateMatrix = false;
    }
}

/**
 * @brief Applies the adequate projecton matrix to the specified shader, window manager wrapper updates them all.
 * Do NOT add the framebuffer shaders to the function
 *
 * @param shader The shader to be updated
 * @param mode Camera mode to use (default or overlay)
 * @return nothing
 */
void SGL_Camera::cameraMode(SGL_Shader shader, CAMERA_MODE mode)
{
    if (mode == DEFAULT)
    {
        // Camera is active, apply the offset to the projection matrix
        shader.use(*WMOGLM).setMatrix4(*WMOGLM, "projection", pCameraMatrix);
    }
    else if (mode == OVERLAY)
    {
        // Camera is disabled, apply the original overlay  matrix
        shader.use(*WMOGLM).setMatrix4(*WMOGLM, "projection", pOverlayMatrix);
    }

}

/**
 * @brief Position the camera
 *
 * @param pos GLM::vec2 (x, y)
 * @return nothing
 */
void SGL_Camera::setPosition(glm::vec2 pos)
{
    this->pCameraPos = pos;
}

/**
 * @brief Returns the camera matrix
 *
 * @return glm::mat4 The camera matrix
 */
glm::mat4 SGL_Camera::getCameraMatrix()
{
    return this->pCameraMatrix;
}


/**
 * @brief Returns the overlay matrix
 *
 * @return glm::mat4 The overlay mode matrix
 */
glm::mat4 SGL_Camera::getOverlayMatrix()
{
    return this->pOverlayMatrix;
}


/**
 * @brief Returns the overlay matrix
 *
 * @return glm::mat4 The overlay mode matrix
 */
glm::vec4 SGL_Camera::getPosition()
{
    return glm::vec4(this->pCameraPos.x, this->pCameraPos.y, pScreenW, pScreenH);
}
