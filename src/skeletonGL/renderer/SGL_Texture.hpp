//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.


/**
 * @file    src/skeletonGL/utility/SGL_Texture.hpp
 * @author  AlexHG
 * @date    9/4/2018
 * @version 1.0
 *
 * @brief Wraps around all necessary texture functionality
 *
 * @section DESCRIPTION
 *
 * Encapsulates all the required texture functionality
 */


#ifndef SRC_SKELETONGL_RENDERER_TEXTURE2D_H
#define SRC_SKELETONGL_RENDERER_TEXTURE2D_H

// C++
#include <string>
// Dependencies
#include <GL/glew.h>
#include <SDL2/SDL.h>
// SkeletonGL
#include "SGL_OpenGLManager.hpp"


/**
 * @brief Defines an SGL_Texture to be used by a sprite
 */
class SGL_Texture
{
public:
    GLuint ID;                                 ///< Internal OpenGL manager identifier
    GLuint width, height;                      ///< Texture dimensions
    GLuint internalFormat;                     ///< Texture internal format
    GLuint imageFormat;                        ///< Image file format
    GLuint wrapS, wrapT, filterMin, filterMax; ///< Texture settings

    // Constructor
    SGL_Texture();

    // Destructor
    ~SGL_Texture();

    // Generates an SGL_Texture
    void generate(SGL_OpenGLManager &oglm, GLuint width, GLuint height, unsigned char *data);

    // Activates the texture
    void bind(SGL_OpenGLManager &oglm) const;

};

#endif // SRC_SKELETONGL_RENDERER_TEXTURE2D_H
